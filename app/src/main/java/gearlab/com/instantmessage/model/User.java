package gearlab.com.instantmessage.model;

/**
 * Created by ivanphytsyk on 2/26/17.
 */

public class User {
    private long id;
    private String name;
    private String pass; //TODO probably it's not a best way to save user password
    private String email;
    private String country;
    private String playedGame;
    private String avatar; //TODO need to be implemented
    private long lastActionTime; //timestamp to check online status

    public User() {

    }

    public User(long id, String playedGame, String name, String pass, String email, String country) {
        this.playedGame = playedGame;
        this.id = id;
        this.name = name;
        this.pass = pass;
        this.email = email;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPlayedGame() {
        return playedGame;
    }

    public void setPlayedGame(String playedGame) {
        this.playedGame = playedGame;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getLastActionTime() {
        return lastActionTime;
    }

    public void setLastActionTime(long lastActionTime) {
        this.lastActionTime = lastActionTime;
    }
}
