package gearlab.com.instantmessage.model;

/**
 * Created by ivanphytsyk on 2/28/17.
 */

public class Message {
    private long senderId;
    private long recipientId;
    private long time;
    private String text;

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (senderId != message.senderId) return false;
        if (recipientId != message.recipientId) return false;
        if (time != message.time) return false;
        return text.equals(message.text);

    }

    @Override
    public int hashCode() {
        int result = (int) (senderId ^ (senderId >>> 32));
        result = 31 * result + (int) (recipientId ^ (recipientId >>> 32));
        result = 31 * result + (int) (time ^ (time >>> 32));
        result = 31 * result + text.hashCode();
        return result;
    }
}
