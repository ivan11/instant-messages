package gearlab.com.instantmessage.session;

import android.content.Context;
import android.content.SharedPreferences;

import gearlab.com.instantmessage.model.User;

/**
 * Created by ivanphytsyk on 2/27/17.
 */

public class Session {

    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_GAME = "user_game";
    private static final String KEY_USER_NAME = "user_name";

    private SharedPreferences sharedPreferences;

    private Session(Context context) {
        sharedPreferences = context.getSharedPreferences("IM", Context.MODE_PRIVATE);
    }

    public static Session createSession(Context context) {
        return new Session(context);
    }

    public long getCurrentUserId() {
        return sharedPreferences.getLong(KEY_USER_ID, 0);
    }

    public void setCurrentUser(Context context, User currentUser) {
        sharedPreferences.edit().putLong(KEY_USER_ID, currentUser.getId())
                .putString(KEY_USER_GAME, currentUser.getPlayedGame())
                .putString(KEY_USER_NAME, currentUser.getName())
                .apply();
    }

    public boolean isAlive() {
        return getCurrentUserId() != 0l;
    }

    public String getGame() {
        return sharedPreferences.getString(KEY_USER_GAME, "");
    }
}
