package gearlab.com.instantmessage.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class HelveticaTextView extends TextView {
    public HelveticaTextView(Context context) {
        super(context);
        initType();
    }

    public HelveticaTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType();
    }

    public HelveticaTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initType();
    }

    private void initType() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                "HelveticaNeue Medium.ttf");
        setTypeface(face);
    }
}
