package gearlab.com.instantmessage.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class HelveticaEditText extends EditText {
    public HelveticaEditText(Context context) {
        super(context);
        initType();
    }

    public HelveticaEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType();
    }

    public HelveticaEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initType();
    }


    private void initType() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(),
                "HelveticaNeue Medium.ttf");
        setTypeface(face);
    }
}
