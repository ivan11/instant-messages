package gearlab.com.instantmessage.repository;

import android.text.TextUtils;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import gearlab.com.instantmessage.model.Message;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.session.Session;

/**
 * Created by ivanphytsyk on 2/27/17.
 */

public class RemoteRepository {

    private static final String USERS_DB = "users";
    private static final String MESSAGES_DB = "messages";

    private FirebaseDatabase firebaseDatabase;
    private Session session;
    private ChildEventListener chatUpdateListener;

    public RemoteRepository(Session session) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        this.session = session;
    }

    public void isUserExists(final String userName, final ReturnValueCallback<Boolean> returnValueCallback) {
        firebaseDatabase.getReference()
                .getRoot().child(USERS_DB)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        boolean exists = dataSnapshot.child(userName).exists();
                        returnValueCallback.onValueReturn(exists);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        returnValueCallback.onValueReturn(false);
                    }
                });
    }

    public void login(final String userName, final String password, final ReturnValueCallback<User> returnValueCallback) {
        firebaseDatabase.getReference()
                .getRoot().child(USERS_DB)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = null;
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            user = snapshot.getValue(User.class);
                            if (user.getName().equals(userName)) {
                                if (!TextUtils.isEmpty(user.getPass())) {
                                    if (user.getPass().equals(password)) {
                                        returnValueCallback.onValueReturn(user);
                                        updateLastActivity(user.getId());
                                        return;
                                    }
                                } else {
                                    returnValueCallback.onValueReturn(user);
                                    return;
                                }
                            }
                        }
                        if (user == null) {
                            returnValueCallback.onValueReturn(null);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        returnValueCallback.onValueReturn(null);
                    }
                });
    }

    public void createUser(final User user) {
        firebaseDatabase.getReference()
                .getRoot().child(USERS_DB)
                .child(String.valueOf(user.getId()))
                .setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                updateLastActivity(user.getId());
            }
        });
    }

    private void updateLastActivity() {
        if (session.isAlive()) {
            long currentUserId = session.getCurrentUserId();
            updateLastActivity(currentUserId);
        }
    }

    private void updateLastActivity(long userId) {
        firebaseDatabase.getReference()
                .getRoot().child(USERS_DB)
                .child(String.valueOf(userId))
                .child("lastActionTime")
                .setValue(System.currentTimeMillis());
    }

    public void getUsersForChat(final ReturnValueCallback<List<User>> returnValueCallback) {
        final long currentUserId = session.getCurrentUserId();
        final String currentUserGame = session.getGame();
        firebaseDatabase.getReference().getRoot().child(USERS_DB).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> result = new ArrayList<User>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    User user = child.getValue(User.class);
                    if (user.getId() != currentUserId && user.getPlayedGame().equals(currentUserGame)) {
                        result.add(user);
                    }
                }
                returnValueCallback.onValueReturn(result);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                returnValueCallback.onValueReturn(new ArrayList<User>());
            }
        });
        updateLastActivity();
    }

    public void getUserById(long userId, final ReturnValueCallback<User> returnValueCallback) {
        firebaseDatabase.getReference()
                .getRoot().child(USERS_DB)
                .child(String.valueOf(userId))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        user.setPass("");
                        returnValueCallback.onValueReturn(user);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        returnValueCallback.onValueReturn(null);
                    }
                });
    }


    public void loadMessages(long chatMateId, final ReturnValueCallback<List<Message>> returnValueCallback) {
        if (session.isAlive()) {
            long currentUserId = session.getCurrentUserId();
            firebaseDatabase
                    .getReference()
                    .getRoot()
                    .child(MESSAGES_DB)
                    .child(String.valueOf(chatMateId + currentUserId))
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            List<Message> messages = new ArrayList<Message>();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                Message message = snapshot.getValue(Message.class);
                                messages.add(message);
                            }
                            returnValueCallback.onValueReturn(messages);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            returnValueCallback.onValueReturn(new ArrayList<Message>());
                        }
                    });
        }
    }

    public void sendMessage(Message message, final ReturnValueCallback<Boolean> returnValueCallback) {
        String chatId = String.valueOf(message.getRecipientId() + message.getSenderId());
        firebaseDatabase
                .getReference()
                .getRoot()
                .child(MESSAGES_DB)
                .child(chatId)
                .child(String.valueOf(message.getTime()))
                .setValue(message)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        returnValueCallback.onValueReturn(true);
                    }
                });
    }

    public void unSubscribeFromChatUpdate(long chatMateId) {
        String chatId = String.valueOf(chatMateId + session.getCurrentUserId());
        firebaseDatabase
                .getReference()
                .getRoot()
                .child(MESSAGES_DB)
                .child(chatId)
                .removeEventListener(chatUpdateListener);
        chatUpdateListener = null;
    }

    public void subscribeForChatUpdate(long chatMateId, final ReturnValueCallback<Message> returnValueCallback) {
        String chatId = String.valueOf(chatMateId + session.getCurrentUserId());
        chatUpdateListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message newMessage = dataSnapshot.getValue(Message.class);
                returnValueCallback.onValueReturn(newMessage);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        firebaseDatabase
                .getReference()
                .getRoot()
                .child(MESSAGES_DB)
                .child(chatId)
                .addChildEventListener(chatUpdateListener);
    }


    public interface ReturnValueCallback<T> {
        public void onValueReturn(T returnedValue);
    }
}
