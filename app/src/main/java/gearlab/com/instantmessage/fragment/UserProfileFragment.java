package gearlab.com.instantmessage.fragment;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import gearlab.com.instantmessage.MainActivity;
import gearlab.com.instantmessage.R;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;

/**
 * Created by ivanphytsyk on 3/1/17.
 */

public class UserProfileFragment extends Fragment {

    public static final String ARG_USER_ID = "arg_user_id";

    private ImageView userAvatar;
    private TextView userName;
    private TextView userGame;
    private TextView userEmail;

    private long userId;
    private RemoteRepository remoteRepository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = getArguments().getLong(ARG_USER_ID);
        remoteRepository = ((MainActivity)getActivity()).getRemoteRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userAvatar = (ImageView) view.findViewById(R.id.user_avatar);
        userName = (TextView) view.findViewById(R.id.user_name);
        userGame = (TextView) view.findViewById(R.id.user_game);
        userEmail = (TextView) view.findViewById(R.id.user_email);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadUserInfo();
    }

    private void loadUserInfo() {
        remoteRepository.getUserById(userId, new RemoteRepository.ReturnValueCallback<User>() {
            @Override
            public void onValueReturn(User user) {
                //userAvatar.setImageURI(Uri.parse(user.getAvatar())); //TODO implement user avatar storing
                userName.setText(user.getName());
                userGame.setText(user.getPlayedGame());
                userEmail.setText(user.getEmail());
            }
        });
    }
}
