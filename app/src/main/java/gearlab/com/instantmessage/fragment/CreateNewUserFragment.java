package gearlab.com.instantmessage.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Random;

import gearlab.com.instantmessage.MainActivity;
import gearlab.com.instantmessage.R;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;
import gearlab.com.instantmessage.session.Session;

/**
 * Created by ivanphytsyk on 2/27/17.
 */

public class CreateNewUserFragment extends Fragment {

    private EditText userNameEditText;
    private EditText passwordEditText;
    private EditText emailEditText;
    private EditText countryEditText;
    private Spinner gamesSpinner;

    private RemoteRepository remoteRepository;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        remoteRepository  = ((MainActivity)getActivity()).getRemoteRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_user, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        gamesSpinner = (Spinner) view.findViewById(R.id.games);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.games_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gamesSpinner.setAdapter(adapter);

        view.findViewById(R.id.create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewUser();
            }
        });

        view.findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToLogin();
            }

        });

        userNameEditText = (EditText) view.findViewById(R.id.user_name);
        emailEditText = (EditText) view.findViewById(R.id.email);
        passwordEditText = (EditText) view.findViewById(R.id.password);
        countryEditText = (EditText) view.findViewById(R.id.country);
    }

    private void navigateToLogin() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginFragment())
                .commit();
    }

    private void createNewUser() {
        String userName = userNameEditText.getText().toString();
        String gameName = (String) gamesSpinner.getSelectedItem();
        String password = passwordEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String country = countryEditText.getText().toString();

        if (TextUtils.isEmpty(userName)) {
            Toast.makeText(getActivity(), "User name is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        final User user = new User(getUserId(), gameName, userName, password, email, country);
        remoteRepository.isUserExists(userName, new RemoteRepository.ReturnValueCallback<Boolean>() {
            @Override
            public void onValueReturn(Boolean exists) {
                if (exists) {
                    Toast.makeText(getActivity(), "User with name exists", Toast.LENGTH_LONG).show();
                } else {
                    remoteRepository.createUser(user);
                    saveCurrentUser(user);
                  //  navigateToChat();
                }
            }
        });
    }

    private void saveCurrentUser(User user) {
        ((MainActivity) getActivity()).getSession().setCurrentUser(getActivity(), user);
    }

    private void navigateToChat() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MessengerFragment())
                .commit();
    }


    private long getUserId() {
        return System.currentTimeMillis();
    }

}
