package gearlab.com.instantmessage.fragment;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import gearlab.com.instantmessage.MainActivity;
import gearlab.com.instantmessage.R;
import gearlab.com.instantmessage.model.Message;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;

/**
 * Created by ivanphytsyk on 2/28/17.
 */

public class PrivateChatFragment extends Fragment {

    public static final String ARG_USER_ID = "arg_user_id";
    private long chatMateId;
    private RemoteRepository remoteRepository;
    private long currentUserId;

    private List<Message> messages = new ArrayList<>();
    private MessagesAdapter adapter;
    private EditText textInput;
    private LinearLayoutManager layoutManager;
    private ImageView userAvatar;
    private TextView userName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatMateId = getArguments().getLong(ARG_USER_ID);
        currentUserId = ((MainActivity) getActivity()).getSession().getCurrentUserId();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        remoteRepository = ((MainActivity) getActivity()).getRemoteRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_private_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        RecyclerView messagesView = (RecyclerView) view.findViewById(R.id.message_list);
        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        messagesView.setLayoutManager(layoutManager);

        adapter = new MessagesAdapter();
        messagesView.setAdapter(adapter);

        userAvatar = (ImageView) view.findViewById(R.id.chat_mate_avatar); //TODO fill when we have user avatar stored on cloud
        userName = (TextView) view.findViewById(R.id.chat_mate_name);

        textInput = (EditText) view.findViewById(R.id.chat_input);

        view.findViewById(R.id.toolbar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToUserProfile();
            }
        });

        view.findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = textInput.getText().toString();
                if (!TextUtils.isEmpty(text)) {
                    sendMessage(text);
                    textInput.setText("");
                }
            }
        });
    }

    private void navigateToUserProfile() {
        Fragment fragment = new UserProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(UserProfileFragment.ARG_USER_ID, chatMateId);
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(UserProfileFragment.class.getName())
                .commit();
    }

    private void sendMessage(final String text) {
        final Message message = new Message();
        message.setSenderId(currentUserId);
        message.setRecipientId(chatMateId);
        message.setTime(System.currentTimeMillis());
        message.setText(text);

        remoteRepository.sendMessage(message, new RemoteRepository.ReturnValueCallback<Boolean>() {
            @Override
            public void onValueReturn(Boolean success) {
                if (success) {
                    Log.d(PrivateChatFragment.class.getName(), "Message sent succesfully: " + text);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        loadMessages();
        loadUserInfo();
        subscribeToUpdate();
    }

    private void loadUserInfo() {
        remoteRepository.getUserById(chatMateId, new RemoteRepository.ReturnValueCallback<User>() {
            @Override
            public void onValueReturn(User user) {
                //userAvatar.setImageURI(Uri.parse(user.getAvatar())); //TODO after user avatar implemented
                userName.setText(user.getName());
            }
        });
    }

    private void subscribeToUpdate() {
        remoteRepository.subscribeForChatUpdate(chatMateId, new RemoteRepository.ReturnValueCallback<Message>() {
            @Override
            public void onValueReturn(Message newMessage) {
                if (!messages.contains(newMessage)) {
                    showNewMessage(newMessage);
                }
            }
        });
    }

    private void showNewMessage(Message message) {
        messages.add(0, message);
        adapter.notifyItemInserted(0);
        layoutManager.scrollToPosition(0);
    }

    private void unsubscribeFromUpdate() {
        remoteRepository.unSubscribeFromChatUpdate(chatMateId);
    }

    @Override
    public void onStop() {
        super.onStop();
        unsubscribeFromUpdate();
    }

    private void loadMessages() {
        remoteRepository.loadMessages(chatMateId, new RemoteRepository.ReturnValueCallback<List<Message>>() {
            @Override
            public void onValueReturn(List<Message> messages) {
                Collections.sort(messages, new Comparator<Message>() {
                    @Override
                    public int compare(Message message, Message message2) {
                        return message.getTime() < message2.getTime() ? 1 : -1;
                    }
                });
                PrivateChatFragment.this.messages = messages;
                adapter.notifyDataSetChanged();
            }
        });
    }

    private class MessageViewHolder extends RecyclerView.ViewHolder {
        private TextView messageText;

        public MessageViewHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.message_text);
        }
    }

    private class MessagesAdapter extends RecyclerView.Adapter<MessageViewHolder> {

        private static final int SELF = 0;
        private static final int CHAT_MATE = 1;

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(getActivity()).inflate(viewType == SELF
                    ? R.layout.item_message_self : R.layout.item_message, parent, false);
            return new MessageViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MessageViewHolder holder, int position) {
            TextView textView = holder.messageText;
            Message message = messages.get(position);
            textView.setText(message.getText());
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        @Override
        public int getItemViewType(int position) {
            return messages.get(position).getSenderId() == currentUserId ? SELF : CHAT_MATE;
        }
    }
}
