package gearlab.com.instantmessage.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import gearlab.com.instantmessage.MainActivity;
import gearlab.com.instantmessage.R;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;

/**
 * Created by ivanphytsyk on 2/27/17.
 */

public class MessengerFragment extends Fragment {

    private static long ONLINE_STATUS_TIME = 60 * 1000; //User is online in 1 minute after his last action

    private RecyclerView userList;

    private List<User> users = new ArrayList<>();
    private UserAdapter userAdapter;
    private RemoteRepository remoteRepository;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        remoteRepository = ((MainActivity) getActivity()).getRemoteRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_messenger, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        userList = (RecyclerView) view.findViewById(R.id.user_list);
        userList.setLayoutManager(new LinearLayoutManager(getActivity()));
        userAdapter = new UserAdapter();
        userList.setAdapter(userAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadUsers();
    }

    private void loadUsers() {
        remoteRepository.getUsersForChat(new RemoteRepository.ReturnValueCallback<List<User>>() {
            @Override
            public void onValueReturn(List<User> returnedValue) {
                Collections.sort(returnedValue, new Comparator<User>() {
                    @Override
                    public int compare(User user, User user2) {
                        return user.getLastActionTime() < user2.getLastActionTime() ? 1 : -1;
                    }
                });
                users = returnedValue;
                userAdapter.notifyDataSetChanged();
            }
        });
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        private ImageView avatarView;
        private TextView userNameView;
        private ImageView onlineStatusView;

        public UserViewHolder(View itemView) {
            super(itemView);
            avatarView = (ImageView) itemView.findViewById(R.id.user_avatar);
            userNameView = (TextView) itemView.findViewById(R.id.user_name);
            onlineStatusView = (ImageView) itemView.findViewById(R.id.online_status);
        }
    }

    private class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {

        @Override
        public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View rootView = LayoutInflater.from(getActivity()).inflate(R.layout.item_messanger_user, parent, false);
            return new UserViewHolder(rootView);
        }

        @Override
        public void onBindViewHolder(UserViewHolder holder, int position) {
            final User user = users.get(position);
            // holder.avatarView.setImageResource(); ;//TODO bind user avatar, implement user avatar storage on cloud
            holder.userNameView.setText(user.getName());
            boolean isOnline = System.currentTimeMillis() - user.getLastActionTime() < ONLINE_STATUS_TIME;
            holder.onlineStatusView.setImageResource(isOnline ? R.drawable.ic_status_online : R.drawable.ic_status_offline);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigateToPrivateChat(user.getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }
    }

    private void navigateToPrivateChat(long id) {
        Fragment fragment = new PrivateChatFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(PrivateChatFragment.ARG_USER_ID, id);
        fragment.setArguments(bundle);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(PrivateChatFragment.class.getName())
                .commit();
    }
}
