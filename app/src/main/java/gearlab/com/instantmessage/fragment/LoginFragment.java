package gearlab.com.instantmessage.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import gearlab.com.instantmessage.MainActivity;
import gearlab.com.instantmessage.R;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;
import gearlab.com.instantmessage.session.Session;

/**
 * Created by ivanphytsyk on 2/27/17.
 */

public class LoginFragment extends Fragment {
    private RemoteRepository remoteRepository;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        remoteRepository  = ((MainActivity)getActivity()).getRemoteRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final EditText userNameEditText = (EditText) view.findViewById(R.id.user_name);
        final EditText passwordEditText = (EditText) view.findViewById(R.id.password);

        view.findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = userNameEditText.getText().toString();
                String password = passwordEditText.getText().toString();

                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
                    Toast.makeText(getActivity(), "User name or password is empty", Toast.LENGTH_SHORT).show();
                    return;
                }

                remoteRepository.login(userName, password, new RemoteRepository.ReturnValueCallback<User>() {
                    @Override
                    public void onValueReturn(User user) {
                        if (user != null) {
                            saveCurrentUser(user);
                            navigateToChat();
                        } else {
                            Toast.makeText(getActivity(), "User not exists", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void saveCurrentUser(User user) {
        ((MainActivity) getActivity()).getSession().setCurrentUser(getActivity(), user);
    }

    private void navigateToChat() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MessengerFragment())
                .commit();
    }
}
