package gearlab.com.instantmessage;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import gearlab.com.instantmessage.fragment.CreateNewUserFragment;
import gearlab.com.instantmessage.fragment.MessengerFragment;
import gearlab.com.instantmessage.model.User;
import gearlab.com.instantmessage.repository.RemoteRepository;
import gearlab.com.instantmessage.session.Session;

public class MainActivity extends Activity {

    private Session session;
    private RemoteRepository remoteRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = Session.createSession(this);
        remoteRepository = new RemoteRepository(session);

        startFirstScreen();
    }
    
    private void startFirstScreen() {
        if (session.isAlive()) {
            navigateToChat();
        } else {
            navigateToUserCreation();
        }
    }

    private void navigateToUserCreation() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, new CreateNewUserFragment())
                .addToBackStack(CreateNewUserFragment.class.getName())
                .commit();

    }

    private void navigateToChat() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new MessengerFragment())
                .commit();
    }

    public Session getSession() {
        return session;
    }

    public RemoteRepository getRemoteRepository() {
        return remoteRepository;
    }
}
